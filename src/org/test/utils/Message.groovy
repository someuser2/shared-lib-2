package org.test.utils

class Message {
    static Map template = [
        'runStatus': ['[${result}] - ${name}\n${url}', Message.&argMap],
        'runStatusShort': ['Job $name completed with status: $result', Message.&argMap],
        'buildDescription': ['<span style="font-family: monospace;color:$color;">$result</span>&nbsp;\n<a href="$url">$shortName</a><br/>\n',
            Message.&argFilter, [[Message.&argMap, Message.&addDataColor], [null, ['result']]] ],
        'hyperionTestReportLink': ['<a href="$url">Hyperion test report</a><br/>\n', Message.&argList, ['url']],
        'error': ['[ERROR] $message', Message.&argList, ['message']],
        'buildDescriptionError': ['<span style="font-family: monospace;color:${color.error};">${message}</span><br>\n',
            Message.&argFilter, [[Message.&argList, Message.&addColor], [['message'],null]] ],
        'warning': ['[WARNING] $message', Message.&argList, ['message']],
        'buildDescriptionWarning': ['<span style="font-family: monospace;color:${color.warning};">${message}</span><br>\n',
            Message.&argFilter, [[Message.&argList, Message.&addColor], [['message'],null]] ],
        'info': ['[INFO] ${message}', Message.&argList, ['message']],
        'buildDescriptionInfo': ['<span style="font-family: monospace;color:${color.info};">${message}</span><br>\n',
            Message.&argFilter, [[Message.&argList, Message.&addColor], [['message'],null]] ],
        'colorizedText': ['${style.fg.green+style.blinking+style.inverse}${message}${style.reset}',
            Message.&argFilter, [[Message.&argList, Message.&addTextStyle], [['message'],null]] ],
    ]

    static final ansiCodeStart =  "${(char)27}["
    static final ansiCodeStop = 'm'
    static final Map textStyle = [
        'fg':[
            'black':   "${ansiCodeStart}30${ansiCodeStop}",
            'red':     "${ansiCodeStart}31${ansiCodeStop}",
            'green':   "${ansiCodeStart}32${ansiCodeStop}",
            'yellow':  "${ansiCodeStart}33${ansiCodeStop}",
            'blue':    "${ansiCodeStart}34${ansiCodeStop}",
            'magenta': "${ansiCodeStart}35${ansiCodeStop}",
            'cyan':    "${ansiCodeStart}36${ansiCodeStop}",
            'white':   "${ansiCodeStart}37${ansiCodeStop}",
            'bright': [
                'black':   "${ansiCodeStart}90${ansiCodeStop}",
                'red':     "${ansiCodeStart}91${ansiCodeStop}",
                'green':   "${ansiCodeStart}92${ansiCodeStop}",
                'yellow':  "${ansiCodeStart}93${ansiCodeStop}",
                'blue':    "${ansiCodeStart}94${ansiCodeStop}",
                'magenta': "${ansiCodeStart}95${ansiCodeStop}",
                'cyan':    "${ansiCodeStart}96${ansiCodeStop}",
                'white':   "${ansiCodeStart}97${ansiCodeStop}"]],
        'bg':[
            'black':   "${ansiCodeStart}40${ansiCodeStop}",
            'red':     "${ansiCodeStart}41${ansiCodeStop}",
            'green':   "${ansiCodeStart}42${ansiCodeStop}",
            'yellow':  "${ansiCodeStart}43${ansiCodeStop}",
            'blue':    "${ansiCodeStart}44${ansiCodeStop}",
            'magenta': "${ansiCodeStart}45${ansiCodeStop}",
            'cyan':    "${ansiCodeStart}46${ansiCodeStop}",
            'white':   "${ansiCodeStart}47${ansiCodeStop}",
            'bright': [
                'black':   "${ansiCodeStart}100${ansiCodeStop}",
                'red':     "${ansiCodeStart}101${ansiCodeStop}",
                'green':   "${ansiCodeStart}102${ansiCodeStop}",
                'yellow':  "${ansiCodeStart}103${ansiCodeStop}",
                'blue':    "${ansiCodeStart}104${ansiCodeStop}",
                'magenta': "${ansiCodeStart}105${ansiCodeStop}",
                'cyan':    "${ansiCodeStart}106${ansiCodeStop}",
                'white':   "${ansiCodeStart}107${ansiCodeStop}"]],
        'reset': "${ansiCodeStart}0${ansiCodeStop}",
        'underline': "${ansiCodeStart}4${ansiCodeStop}",
        'bold': "${ansiCodeStart}1${ansiCodeStop}",
        'crossed': "${ansiCodeStart}9${ansiCodeStop}",
        'italic': "${ansiCodeStart}3${ansiCodeStop}",
        'inverse': "${ansiCodeStart}7${ansiCodeStop}",
        'blinking': "${ansiCodeStart}5${ansiCodeStop}",
    ]

    static final Map color = [
        'error': '#ff0000',
        'warning': '#ff9900',
        'info': '#505050',
        'FAILURE': '#ff0000',
        'ABORTED': '#ff9900',
        'UNSTABLE': '#ff9900',
        'SUCCESS': '#339966']

    static def $static_methodMissing(String name, Object args) {
        if(template[name] && template[name][0] && template[name][0] instanceof String) {
            Map data = template[name][1] ? template[name][1](args, template[name][2]) : [:]
            templateToString(template[name][0], data)
        } else {
            throw new MissingMethodException(name, getClass(), args)
        }
    }

    static String templateToString(String template, Map data) {
        new groovy.text.GStringTemplateEngine().createTemplate(template).make(data).toString()
    }

    static Map argMap(Object data, Object closureArgs=null) {
        data[0]
    }

    static Map argList(Object data, List closureArgs) {
        Map result = [:]
        for(int i=0; i < closureArgs.size(); i++) {
            if (i<data.size()) {
                result[closureArgs[i]] = data[i]
            } else {
                throw new IllegalArgumentException("Missing value for ${closureArgs[i]}")
            }
        }
        result
    }

    static Map argFilter(Object data, Object args) {
        List closures = args[0]
        List closureArgs = args[1]
        Map result = closures[0](data, closureArgs[0])
        for(int i=1; i < closures.size(); i++) {
            result = closures[i](result, ['data': data, 'args': closureArgs[i]])
        }
        result
    }

    static Map addColor(Map data, Object args=null) {
        data + ['color': Message.color]
    }

    static Map addTextStyle(Map data, Object args=null) {
        data + ['style': Message.textStyle]
    }

    static Map addDataColor(Map data, Object args=null) {
        data + ['color': Message.color[args.data[args.args[0]][0]]]
    }

    static String textStylesDemo(Map data, String text=''){
        String result=''
        data.each { name, value ->
            if(value instanceof java.util.LinkedHashMap) {
                result += textStylesDemo(value,"${text}.${name}")
            } else {
                result += "${value}${text}.${name}${Message.textStyle.reset}\n"
                }
        }
        result
    }
}
